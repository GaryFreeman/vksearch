﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VKSearch.Data
{
	[Serializable]
	internal struct BoardCommentId
	{
		public BoardCommentId(long id, long topicId, long communityId)
		{
			Id = id;
			TopicId = topicId;
			CommunityId = communityId;
		}

		public long CommunityId
		{
			get;
		}

		public long Id
		{
			get;
		}

		public long TopicId
		{
			get;
		}

		public static bool operator !=(BoardCommentId x, BoardCommentId y)
		{
			return !(x == y);
		}

		public static bool operator ==(BoardCommentId x, BoardCommentId y)
		{
			return x.Equals(y);
		}

		public override bool Equals(object obj)
		{
			if (obj is BoardCommentId) {
				var boardCommentId = (BoardCommentId)obj;
				return Id == boardCommentId.Id && TopicId == boardCommentId.TopicId && CommunityId == boardCommentId.CommunityId;
			}

			return false;
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode() ^ TopicId.GetHashCode() ^ CommunityId.GetHashCode();
		}
	}
}