﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKSearch.Data
{
	internal enum Status
	{
		Paused,
		PreliminarySearch,
		SearchingInCommunities,
		SearchingInNewsFeed,
		Stopped
	}
}