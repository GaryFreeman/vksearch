﻿using System;

namespace VKSearch.Data
{
	internal class Filtering
	{
		[Flags]
		public enum Attachments
		{
			None = 0x0,
			Album = 0x1,
			Audio = 0x2,
			Photo = 0x4,
			Video = 0x8
		}

		public string[] BoardTopicsSearchQueries
		{
			get;
			set;
		}

		public string[] Exceptions
		{
			get;
			set;
		}

		public DateTime SkipOlderThan
		{
			get;
			set;
		}

		public Attachments SkipWithAttachments
		{
			get;
			set;
		}
	}
}