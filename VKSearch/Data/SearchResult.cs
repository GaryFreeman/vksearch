﻿namespace VKSearch.Data
{
	internal class SearchResult
	{
		public enum ResultCode
		{
			Cancelled,
			CaptchaFailed,
			IncorrectLoginOrPassword,
			TooManyRequests,
			UnknownError,
			WorkCompleted
		}

		public ResultCode Code
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}
	}
}