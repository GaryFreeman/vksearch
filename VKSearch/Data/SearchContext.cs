﻿using System;
using System.IO;

namespace VKSearch.Data
{
	internal class SearchContext
	{
		public string[] BoardCommentsSearchQueries
		{
			get;
			set;
		}

		public long? CityId
		{
			get;
			set;
		}

		public string[] CommunitiesSearchQueries
		{
			get;
			set;
		}

		public long? CountryId
		{
			get;
			set;
		}

		public FileInfo DumpFile
		{
			get;
			set;
		}

		public Filtering Filtering
		{
			get;
			set;
		}

		public bool Loop
		{
			get;
			set;
		}

		public string[] NewsFeedSearchQueries
		{
			get;
			set;
		}

		public bool PreventDuplicates
		{
			get;
			set;
		}

		public SearchItems SearchItems
		{
			get;
			set;
		}

		public string[] WallCommentsSearchQueries
		{
			get;
			set;
		}

		public string[] WallPostsSearchQueries
		{
			get;
			set;
		}
	}
}