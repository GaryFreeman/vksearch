﻿using System;

namespace VKSearch.Data
{
	[Serializable]
	internal struct VkId
	{
		public VkId(long id, long ownerId)
		{
			Id = id;
			OwnerId = ownerId;
		}

		public long Id
		{
			get;
		}

		public long OwnerId
		{
			get;
		}

		public static bool operator !=(VkId x, VkId y)
		{
			return !(x == y);
		}

		public static bool operator ==(VkId x, VkId y)
		{
			return x.Equals(y);
		}

		public override bool Equals(object obj)
		{
			if (obj is VkId) {
				var vkId = (VkId)obj;
				return Id == vkId.Id && OwnerId == vkId.OwnerId;
			}

			return false;
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode() ^ OwnerId.GetHashCode();
		}

		public override string ToString()
		{
			return $"{OwnerId}_{Id}";
		}
	}
}