﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKSearch.Data
{
	[Serializable]
	internal class FoundClients
	{
		public HashSet<BoardCommentId> FoundBoardComments
		{
			get;
			set;
		}

		public HashSet<VkId> FoundWallComments
		{
			get;
			set;
		}

		public HashSet<VkId> FoundWallPosts
		{
			get;
			set;
		}
	}
}