﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKSearch.Data
{
	[Flags]
	internal enum SearchItems
	{
		None = 0x0,
		BoardComments = 0x1,
		WallPosts = 0x2,
		WallComments = 0x4,
		NewsFeed = 0x8
	}
}