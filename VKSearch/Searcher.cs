﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows;
using Newtonsoft.Json.Linq;
using VkNet;
using VkNet.Enums;
using VkNet.Enums.Filters;
using VkNet.Enums.SafetyEnums;
using VkNet.Exception;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;
using VkNet.Utils;
using VKSearch.Data;
using VKSearch.Events;
using VKSearch.Misc;
using Status = VKSearch.Data.Status;

namespace VKSearch
{
    internal class Searcher : ISearcher, IDisposable
    {
        private readonly BackgroundWorker _backgroundWorker = new BackgroundWorker {
            WorkerSupportsCancellation = true,
        };

        private readonly Dictionary<long, long> _boardTopicToLastComment = new Dictionary<long, long>();
        private readonly List<long> _foundCommunities = new List<long>();
        private readonly VkApi _vk;
        private StreamWriter _dumpFileWriter;
        private HashSet<BoardCommentId> _foundBoardComments = new HashSet<BoardCommentId>();
        private HashSet<VkId> _foundWallComments = new HashSet<VkId>();
        private HashSet<VkId> _foundWallPosts = new HashSet<VkId>();
        private Data.Status _status = Data.Status.Stopped;

        public Searcher(Window mainWindow)
        {
            _backgroundWorker.DoWork += DoWork;
            _backgroundWorker.RunWorkerCompleted += OnWorkComplete;

            var captchaSolver = new CaptchaSolver(mainWindow);
            _vk = new VkApi(captchaSolver);
        }

        public event FoundClientsCountChangedEventHandler FoundClientsCountChanged;

        public event ScannedItemsCountChangedEventHandler ScannedItemsCountChanged;

        public event SearchCompletedEventHandler SearchCompleted;

        public event StatusChangedEventHandler StatusChanged;

        public string AccountName
        {
            get
            {
                var profileInfo = _vk.Account.GetProfileInfo();
                var maidenName = "";
                if (!String.IsNullOrEmpty(profileInfo.MaidenName)) {
                    maidenName = " " + profileInfo.MaidenName;
                }

                return profileInfo.FirstName + maidenName + " " + profileInfo.LastName;
            }
        }

        public SearchContext Context
        {
            get;
            private set;
        }

        public VkCollection<Country> Countries => _vk.Database.GetCountries();

        public bool IsAuthorized => _vk.IsAuthorized;

        public Data.Status Status
        {
            get
            {
                return _status;
            }
            private set
            {
                var oldStatus = Status;
                _status = value;

                RaiseStatusChanged(oldStatus, Status);
            }
        }

        public void Authorize(string accessToken, long userId)
        {
            _vk.Authorize(accessToken);
            _vk.UserId = userId;
        }

        public void Dispose()
        {
            _dumpFileWriter?.Close();
        }

        public VkCollection<City> GetCities(int countryId)
        {
            return _vk.Database.GetCities(countryId);
        }

        public void Pause()
        {
            throw new NotImplementedException();
        }

        public void Start(SearchContext context)
        {
            Context = context;

            if (!_vk.IsAuthorized) {
                return;
            }

            if (Context.DumpFile != null) {
                _dumpFileWriter = Context.DumpFile.AppendText();
            }

            if (Context.PreventDuplicates) {
                var foundClientsFile = new FileInfo("found_clients.dat");
                if (foundClientsFile.Exists) {
                    using (var stream = foundClientsFile.OpenRead()) {
                        var formatter = new BinaryFormatter();

                        var foundClients = formatter.Deserialize(stream) as FoundClients;
                        _foundBoardComments = foundClients.FoundBoardComments;
                        _foundWallComments = foundClients.FoundWallComments;
                        _foundWallPosts = foundClients.FoundWallPosts;
                    }
                }
            } else {
                _foundBoardComments.Clear();
                _foundWallComments.Clear();
                _foundWallPosts.Clear();
            }

            _backgroundWorker.RunWorkerAsync();
        }

        public void Stop()
        {
            _backgroundWorker.CancelAsync();
        }

        private bool CheckBoardComment(BoardComment comment)
        {
            var id = new BoardCommentId(comment.Id, comment.TopicId, comment.CommunityId);
            if (_foundBoardComments.Contains(id)) {
                return false;
            }

            if (comment.Date.Value < Context.Filtering.SkipOlderThan) {
                return false;
            }

            if (Context.Filtering.Exceptions.Any(exception => comment.Text.Contains(exception, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            foreach (var attachment in comment.Attachments) {
                if (attachment.Type == typeof(Album) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Album)) {
                    return false;
                }
                if (attachment.Type == typeof(Audio) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Audio)) {
                    return false;
                }
                if (attachment.Type == typeof(Photo) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Photo)) {
                    return false;
                }
                if (attachment.Type == typeof(Video) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Video)) {
                    return false;
                }
            }

            if (!Context.BoardCommentsSearchQueries.Any(query => comment.Text.Contains(query, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            _foundBoardComments.Add(id);

            return true;
        }

        private bool CheckBoardTopic(BoardTopic boardTopic)
        {
            return Context.Filtering.BoardTopicsSearchQueries.Length == 0 ||
                   Context.Filtering.BoardTopicsSearchQueries.Any(query => boardTopic.Title.Contains(query, StringComparison.OrdinalIgnoreCase));
        }

        private bool CheckNewsItem(NewsItem newsItem)
        {
            var id = new VkId((long)newsItem.PostId, newsItem.SourceId);
            if (_foundWallPosts.Contains(id)) {
                return false;
            }

            if (newsItem.Date.Value < Context.Filtering.SkipOlderThan) {
                return false;
            }

            if (Context.Filtering.Exceptions.Any(exception => newsItem.Text.Contains(exception, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            foreach (var attachment in newsItem.Attachments) {
                if (attachment.Type == typeof(Album) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Album)) {
                    return false;
                }
                if (attachment.Type == typeof(Audio) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Audio)) {
                    return false;
                }
                if (attachment.Type == typeof(Photo) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Photo)) {
                    return false;
                }
                if (attachment.Type == typeof(Video) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Video)) {
                    return false;
                }
            }

            if (!Context.NewsFeedSearchQueries.Any(query => newsItem.Text.Contains(query, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            _foundWallPosts.Add(id);

            return true;
        }

        private bool CheckWallComment(Comment comment, long ownerId)
        {
            var id = new VkId(comment.Id, ownerId);
            if (_foundWallComments.Contains(id)) {
                return false;
            }

            if (comment.Date.Value < Context.Filtering.SkipOlderThan) {
                return false;
            }

            if (Context.Filtering.Exceptions.Any(exception => comment.Text.Contains(exception, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            foreach (var attachment in comment.Attachments) {
                if (attachment.Type == typeof(Album) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Album)) {
                    return false;
                }
                if (attachment.Type == typeof(Audio) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Audio)) {
                    return false;
                }
                if (attachment.Type == typeof(Photo) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Photo)) {
                    return false;
                }
                if (attachment.Type == typeof(Video) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Video)) {
                    return false;
                }
            }

            if (!Context.WallCommentsSearchQueries.Any(query => comment.Text.Contains(query, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            _foundWallComments.Add(id);

            return true;
        }

        private bool CheckWallPost(Post post)
        {
            var id = new VkId(post.Id.Value, post.OwnerId.Value);
            if (_foundWallPosts.Contains(id)) {
                return false;
            }

            if (post.Date.Value < Context.Filtering.SkipOlderThan) {
                return false;
            }

            if (Context.Filtering.Exceptions.Any(exception => post.Text.Contains(exception, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            foreach (var attachment in post.Attachments) {
                if (attachment.Type == typeof(Album) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Album)) {
                    return false;
                }
                if (attachment.Type == typeof(Audio) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Audio)) {
                    return false;
                }
                if (attachment.Type == typeof(Photo) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Photo)) {
                    return false;
                }
                if (attachment.Type == typeof(Video) && Context.Filtering.SkipWithAttachments.HasFlag(Filtering.Attachments.Video)) {
                    return false;
                }
            }

            if (!Context.WallPostsSearchQueries.Any(query => post.Text.Contains(query, StringComparison.OrdinalIgnoreCase))) {
                return false;
            }

            _foundWallPosts.Add(id);

            return true;
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            try {
                if (MainLoop()) {
                    e.Result = new SearchResult {
                        Code = SearchResult.ResultCode.WorkCompleted
                    };
                } else {
                    e.Cancel = true;
                }
            } catch (CaptchaNeededException) {
                e.Result = new SearchResult {
                    Code = SearchResult.ResultCode.CaptchaFailed
                };
            } catch (TooManyRequestsException) {
                e.Result = new SearchResult {
                    Code = SearchResult.ResultCode.TooManyRequests
                };
            } catch (VkApiException ex) {
                e.Result = new SearchResult {
                    Code = SearchResult.ResultCode.UnknownError,
                    Message = ex.Message
                };
            }

            Status = Status.Stopped;
        }

        private IEnumerable<BoardComment> GetBoardComments(BoardTopic boardTopic, Group community)
        {
            var offset = 0;
            VkResponse response;

            do {
                var @params = new VkParameters {
                    { "group_id", community.Id },
                    { "topic_id", boardTopic.Id },
                    { "count", 100 },
                    { "offset", offset },
                    { "start_comment_id", _boardTopicToLastComment.ContainsKey(boardTopic.Id) ? _boardTopicToLastComment[boardTopic.Id] : 0 },
                    { "v", VkApi.VkApiVersion }
                };
                offset += 100;

                var answer = _vk.Invoke("board.getComments", @params, true);
                var json = JObject.Parse(answer);
                var rawResponse = json["response"];

                response = new VkResponse(rawResponse) { RawJson = answer };
                foreach (var comment in (VkResponseArray)response["items"]) {
                    var boardComment = BoardComment.FromJson(comment);
                    boardComment.CommunityId = community.Id;
                    boardComment.TopicId = boardTopic.Id;
                    boardComment.TopicUrl = boardTopic.ToString();

                    _boardTopicToLastComment[boardTopic.Id] = boardComment.Id;

                    yield return boardComment;
                }
            } while (offset < response["count"]);
        }

        private IEnumerable<BoardTopic> GetBoardTopics(Group community)
        {
            var offset = 0;
            VkResponse response;

            do {
                var @params = new VkParameters {
                    { "group_id", community.Id },
                    { "count", 100 },
                    { "offset", offset },
                    { "v", VkApi.VkApiVersion }
                };
                offset += 100;

                var answer = _vk.Invoke("board.getTopics", @params, true);
                var json = JObject.Parse(answer);
                var rawResponse = json["response"];

                response = new VkResponse(rawResponse) { RawJson = answer };
                foreach (var topic in (VkResponseArray)response["items"]) {
                    var boardTopic = BoardTopic.FromJson(topic);
                    boardTopic.OwnerId = -community.Id;

                    yield return boardTopic;
                }
            } while (offset < response["count"]);
        }

        private IEnumerable<NewsItem> GetNewsItems()
        {
            var newsFeedGetParams = new NewsFeedGetParams {
                Count = 100,
                Filters = NewsTypes.Post,
                ReturnBanned = true
            };

            do {
                var newsFeed = _vk.NewsFeed.Get(newsFeedGetParams);
                newsFeedGetParams.StartFrom = newsFeed.NextFrom;

                foreach (var newsItem in newsFeed.Items) {
                    if (newsItem.Date.Value < Context.Filtering.SkipOlderThan) {
                        yield break;
                    }

                    yield return newsItem;
                }
            } while (true);
        }

        private IEnumerable<Comment> GetPostComments(Post post)
        {
            var wallGetCommentsParams = new WallGetCommentsParams {
                OwnerId = post.OwnerId,
                Count = 100,
                Offset = 0,
                PostId = post.Id.Value,
                PreviewLength = 0
            };

            VkCollection<Comment> comments;
            do {
                comments = _vk.Wall.GetComments(wallGetCommentsParams, true);
                wallGetCommentsParams.Offset += comments.Count;

                foreach (var comment in comments) {
                    yield return comment;
                }
            } while ((ulong)wallGetCommentsParams.Offset.Value < comments.TotalCount);
        }

        private IEnumerable<Post> GetWallPosts(Group community)
        {
            var wallGetParams = new WallGetParams {
                OwnerId = -community.Id,
                Filter = WallFilter.All,
                Count = 100,
                Offset = 0
            };

            WallGetObject posts;
            do {
                try {
                    posts = _vk.Wall.Get(wallGetParams);
                } catch (AccessDeniedException) {
                    // Стена отключена.

                    yield break;
                }

                wallGetParams.Offset += (ulong)posts.WallPosts.Count;

                foreach (var post in posts.WallPosts) {
                    if (post.Date.Value < Context.Filtering.SkipOlderThan) {
                        yield break;
                    }

                    yield return post;
                }
            } while (wallGetParams.Offset < posts.TotalCount);
        }

        private bool MainLoop()
        {
            var bypass = 1;
            var foundClientsCount = 0;

            ICollection<Group> communities = null;
            do {
                if (_backgroundWorker.CancellationPending) {
                    return false;
                }

                var searchNewsFeed = Context.SearchItems.HasFlag(SearchItems.NewsFeed);
                if (searchNewsFeed && Context.Filtering.SkipOlderThan != DateTime.MinValue) {
                    Status = Data.Status.SearchingInNewsFeed;

                    var i = 0;
                    foreach (var newsItem in GetNewsItems()) {
                        RaiseScannedItemsCountChanged(i++, Int32.MaxValue, bypass);

                        if (_backgroundWorker.CancellationPending) {
                            return false;
                        }

                        if (CheckNewsItem(newsItem)) {
                            SendNewsItem(newsItem);
                            RaiseFoundClientsCountChanged(++foundClientsCount);
                        }
                    }
                }

                if (_backgroundWorker.CancellationPending) {
                    return false;
                }

                var searchWallPosts = Context.SearchItems.HasFlag(SearchItems.WallPosts);
                var searchWallComments = Context.SearchItems.HasFlag(SearchItems.WallComments);
                var searchBoardComments = Context.SearchItems.HasFlag(SearchItems.BoardComments);

                if (searchWallPosts || searchWallComments || searchBoardComments) {
                    if (communities == null) {
                        Status = Data.Status.PreliminarySearch;
                        communities = SearchCommunities();
                    }

                    Status = Data.Status.SearchingInCommunities;
                    RaiseScannedItemsCountChanged(0, communities.Count, bypass);

                    var i = 0;
                    foreach (var community in communities) {
                        RaiseScannedItemsCountChanged(i++, communities.Count, bypass);

                        if (_backgroundWorker.CancellationPending) {
                            return false;
                        }

                        if (community.IsAdmin || (community.IsClosed != null && community.IsClosed.Value != GroupPublicity.Public) ||
                            (community.Deactivated != null && community.Deactivated != Deactivated.Activated)) {
                            continue;
                        }

                        if (searchWallPosts || searchWallComments) {
                            foreach (var post in GetWallPosts(community)) {
                                if (_backgroundWorker.CancellationPending) {
                                    return false;
                                }

                                if (searchWallPosts && CheckWallPost(post)) {
                                    SendWallPost(post, community);
                                    RaiseFoundClientsCountChanged(++foundClientsCount);
                                }

                                if (searchWallComments && post.Comments.Count > 0) {
                                    foreach (var comment in GetPostComments(post)) {
                                        if (_backgroundWorker.CancellationPending) {
                                            return false;
                                        }

                                        if (CheckWallComment(comment, community.Id)) {
                                            SendWallComment(comment, post, community);
                                            RaiseFoundClientsCountChanged(++foundClientsCount);
                                        }
                                    }
                                }
                            }
                        }

                        if (searchBoardComments) {
                            foreach (var boardTopic in GetBoardTopics(community)) {
                                if (_backgroundWorker.CancellationPending) {
                                    return false;
                                }

                                if (!CheckBoardTopic(boardTopic)) {
                                    continue;
                                }

                                foreach (var comment in GetBoardComments(boardTopic, community)) {
                                    if (_backgroundWorker.CancellationPending) {
                                        return false;
                                    }

                                    if (CheckBoardComment(comment)) {
                                        SendBoardComment(comment, community);
                                        RaiseFoundClientsCountChanged(++foundClientsCount);
                                    }
                                }
                            }
                        }

                        _dumpFileWriter?.Flush();
                    }
                }

                ++bypass;
            } while (Context.Loop);

            return true;
        }

        private void OnWorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            var searchResult = new SearchResult();
            if (e.Error != null) {
                searchResult.Code = SearchResult.ResultCode.UnknownError;
                searchResult.Message = e.Error.Message;
            } else if (e.Cancelled) {
                searchResult.Code = SearchResult.ResultCode.Cancelled;
            } else {
                searchResult = e.Result as SearchResult;
            }

            _dumpFileWriter?.Close();

            var foundClients = new FoundClients {
                FoundBoardComments = new HashSet<BoardCommentId>(_foundBoardComments),
                FoundWallComments = new HashSet<VkId>(_foundWallComments),
                FoundWallPosts = new HashSet<VkId>(_foundWallPosts)
            };

            var formatter = new BinaryFormatter();
            var foundClientsFile = new FileInfo("found_clients.dat");
            if (!Context.PreventDuplicates) {
                // Если перед запуском поиска данные о предыдущих клиентах не были загружены, необходимо загрузить их
                // сейчас и объединить с клиентами, найденными в этот раз.

                if (foundClientsFile.Exists) {
                    using (var stream = foundClientsFile.OpenRead()) {
                        var oldFoundClients = formatter.Deserialize(stream) as FoundClients;
                        foundClients.FoundBoardComments.UnionWith(oldFoundClients.FoundBoardComments);
                        foundClients.FoundWallComments.UnionWith(oldFoundClients.FoundWallComments);
                        foundClients.FoundWallPosts.UnionWith(oldFoundClients.FoundWallPosts);
                    }
                }
            }

            using (var stream = foundClientsFile.OpenWrite()) {
                formatter.Serialize(stream, foundClients);
            }

            RaiseSearchCompleted(searchResult);
        }

        #region Методы вызова событий

        private void RaiseFoundClientsCountChanged(int count)
        {
            FoundClientsCountChanged?.Invoke(this, new FoundClientsCountChangedEventArgs(count));
        }

        private void RaiseScannedItemsCountChanged(int scanned, int totalCount, int bypass)
        {
            ScannedItemsCountChanged?.Invoke(this, new ScannedItemsCountChangedEventArgs(scanned, totalCount, bypass));
        }

        private void RaiseSearchCompleted(SearchResult result)
        {
            SearchCompleted?.Invoke(this, new SearchCompletedEventArgs(result));
        }

        private void RaiseStatusChanged(Data.Status oldStatus, Data.Status newStatus)
        {
            StatusChanged?.Invoke(this, new StatusChangedEventArgs(oldStatus, newStatus));
        }

        #endregion Методы вызова событий

        private ICollection<Group> SearchCommunities()
        {
            _foundCommunities.Clear();

            var result = new List<Group>();
            foreach (var communitiesSearchQuery in Context.CommunitiesSearchQueries) {
                if (_backgroundWorker.CancellationPending) {
                    return result;
                }

                var communities = SearchCommunities(communitiesSearchQuery);
                foreach (var community in communities.Where(community => !_foundCommunities.Contains(community.Id))) {
                    if (_backgroundWorker.CancellationPending) {
                        return result;
                    }

                    _foundCommunities.Add(community.Id);
                    result.Add(community);
                }
            }

            return result;
        }

        private IEnumerable<Group> SearchCommunities(string query)
        {
            foreach (var sort in Enum.GetValues(typeof(GroupSort)).OfType<GroupSort>()) {
                if (_backgroundWorker.CancellationPending) {
                    yield break;
                }

                var groupsSearchParams = new GroupsSearchParams {
                    CityId = Context.CityId,
                    Count = 1000,
                    CountryId = Context.CountryId,
                    Query = query,
                    Sort = sort
                };

                var communities = _vk.Groups.Search(groupsSearchParams);
                foreach (var community in communities) {
                    yield return community;
                }
            }
        }

        private IEnumerable<Post> SearchWallComments(Group community, string query)
        {
            var wallSearchParams = new WallSearchParams {
                OwnerId = -community.Id,
                OwnersOnly = false,
                Query = query,
                Count = 100,
                Offset = 0
            };

            VkCollection<Post> comments;
            do {
                comments = _vk.Wall.Search(wallSearchParams);
                wallSearchParams.Offset += (long)comments.Count;

                foreach (var comment in comments.Where(c => c.PostType == PostType.Reply)) {
                    if (comment.Date.Value < Context.Filtering.SkipOlderThan) {
                        yield break;
                    }

                    yield return comment;
                }
            } while ((ulong)wallSearchParams.Offset.Value < comments.TotalCount);
        }

        private void SendBoardComment(BoardComment comment, Group community)
        {
            var message = new String('=', 23) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 23) + "\n";
            var dumpMessage = new String('=', 27) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 27) + "\n";

            if (comment.FromId < 0) {
                // Сообщество

                var authorGroup = _vk.Groups.GetById(-comment.FromId);
                message += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
                dumpMessage += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
            } else {
                // Пользователь

                var authorUser = _vk.Users.Get(comment.FromId);
                var userInfo = $"Автор (пользователь {authorUser.FirstName + (String.IsNullOrEmpty(authorUser.MaidenName) ? "" : " ") + authorUser.MaidenName + " " + authorUser.LastName}): https://vk.com/{(authorUser.ScreenName ?? "id" + authorUser.Id)}\n";
                userInfo += $"Сообщество {community.Name}: https://vk.com/{community.ScreenName}";

                message += userInfo;
                dumpMessage += userInfo;
            }

            message += $"\n\nСсылка на сообщение: https://vk.com/{comment.TopicUrl}?post={comment.Id}";
            dumpMessage += $"\nСсылка на сообщение: https://vk.com/{comment.TopicUrl}?post={comment.Id}";
            dumpMessage += "\n\nТекст сообщения:\n" + comment.Text;

            _vk.Messages.Send(new MessagesSendParams {
                UserId = _vk.UserId,
                Message = message
            });

            _dumpFileWriter?.WriteLine(dumpMessage + "\n\n");
        }

        private void SendNewsItem(NewsItem newsItem)
        {
            var message = new String('=', 23) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 23) + "\n";
            var dumpMessage = new String('=', 27) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 27) + "\n";

            if (newsItem.SourceId < 0) {
                // Сообщество

                var authorGroup = _vk.Groups.GetById(-newsItem.SourceId);
                message += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
                dumpMessage += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
            } else {
                // Пользователь

                var authorUser = _vk.Users.Get(newsItem.SourceId);
                var authorInfo = $"Автор (пользователь {authorUser.FirstName + (String.IsNullOrEmpty(authorUser.MaidenName) ? "" : " ") + authorUser.MaidenName + " " + authorUser.LastName}): https://vk.com/{(authorUser.ScreenName ?? "id" + authorUser.Id)}\n";

                message += authorInfo;
                dumpMessage += authorInfo;
            }

            dumpMessage += "\n\nТекст сообщения:\n" + newsItem.Text;

            var attachments = new List<MediaAttachment> {
                new Post {
                    Id = (long)newsItem.PostId,
                    OwnerId = newsItem.SourceId
                }
            };

            _vk.Messages.Send(new MessagesSendParams {
                Attachments = attachments,
                UserId = _vk.UserId,
                Message = message
            });

            _dumpFileWriter?.WriteLine(dumpMessage + "\n\n");
        }

        private void SendWallComment(Comment comment, Post post, Group community)
        {
            var message = new String('=', 23) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 23) + "\n";
            var dumpMessage = new String('=', 27) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 27) + "\n";

            if (comment.FromId < 0) {
                // Сообщество

                var authorGroup = _vk.Groups.GetById(-comment.FromId);
                message += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
                dumpMessage += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
            } else {
                // Пользователь

                var authorUser = _vk.Users.Get(comment.FromId);
                var userInfo = $"Автор (пользователь {authorUser.FirstName + (String.IsNullOrEmpty(authorUser.MaidenName) ? "" : " ") + authorUser.MaidenName + " " + authorUser.LastName}): https://vk.com/{(authorUser.ScreenName ?? "id" + authorUser.Id)}\n";
                userInfo += $"Сообщество {community.Name}: https://vk.com/{community.ScreenName}";

                message += userInfo;
                dumpMessage += userInfo;
            }

            var postLink = $"\nСсылка на пост: https://vk.com/wall{post.OwnerId}_{post.Id}";
            message += postLink;
            dumpMessage += postLink;

            message += "\n\nТекст сообщения:\n" + comment.Text;
            dumpMessage += "\n\nТекст сообщения:\n" + comment.Text;

            _vk.Messages.Send(new MessagesSendParams {
                UserId = _vk.UserId,
                Message = message
            });

            _dumpFileWriter?.WriteLine(dumpMessage + "\n\n");
        }

        private void SendWallPost(Post post, Group community)
        {
            var message = new String('=', 23) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 23) + "\n";
            var dumpMessage = new String('=', 27) + "\nНАЙДЕН ПОТЕНЦИАЛЬНЫЙ КЛИЕНТ\n" + new String('=', 27) + "\n";

            if (post.FromId != null) {
                if (post.FromId.Value < 0) {
                    // Сообщество

                    var authorGroup = _vk.Groups.GetById(-post.FromId.Value);
                    message += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
                    dumpMessage += $"Автор (сообщество {authorGroup.Name}): https://vk.com/{authorGroup.ScreenName}";
                } else {
                    // Пользователь

                    var authorUser = _vk.Users.Get(post.FromId.Value);
                    var authorInfo = $"Автор (пользователь {authorUser.FirstName + (String.IsNullOrEmpty(authorUser.MaidenName) ? "" : " ") + authorUser.MaidenName + " " + authorUser.LastName}): https://vk.com/{(authorUser.ScreenName ?? "id" + authorUser.Id)}\n";
                    authorInfo += $"Сообщество {community.Name}: https://vk.com/{community.ScreenName}";

                    message += authorInfo;
                    dumpMessage += authorInfo;
                }
            }

            dumpMessage += "\n\nТекст сообщения:\n" + post.Text;

            var attachments = new List<MediaAttachment> {
                post
            };

            _vk.Messages.Send(new MessagesSendParams {
                Attachments = attachments,
                UserId = _vk.UserId,
                Message = message
            });

            _dumpFileWriter?.WriteLine(dumpMessage + "\n\n");
        }
    }
}