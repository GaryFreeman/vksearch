﻿using JetBrains.Annotations;
using VkNet.Model;
using VkNet.Utils;
using VKSearch.Data;
using VKSearch.Events;

namespace VKSearch
{
	internal interface ISearcher
	{
		event FoundClientsCountChangedEventHandler FoundClientsCountChanged;

		event ScannedItemsCountChangedEventHandler ScannedItemsCountChanged;

		event SearchCompletedEventHandler SearchCompleted;

		event StatusChangedEventHandler StatusChanged;

		string AccountName
		{
			get;
		}

		SearchContext Context
		{
			get;
		}

		VkCollection<Country> Countries
		{
			get;
		}

		bool IsAuthorized
		{
			get;
		}

		void Authorize(string accessToken, long userId);

		VkCollection<City> GetCities(int countryId);

		void Pause();

		void Start(SearchContext context);

		void Stop();
	}
}