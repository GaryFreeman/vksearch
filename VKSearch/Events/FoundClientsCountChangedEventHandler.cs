﻿namespace VKSearch.Events
{
	internal delegate void FoundClientsCountChangedEventHandler(ISearcher sender, FoundClientsCountChangedEventArgs e);
}