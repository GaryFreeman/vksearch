﻿namespace VKSearch.Events
{
	internal delegate void ScannedItemsCountChangedEventHandler(ISearcher sender, ScannedItemsCountChangedEventArgs e);
}