﻿using System;
using VKSearch.Data;

namespace VKSearch.Events
{
	internal class SearchCompletedEventArgs : EventArgs
	{
		public SearchCompletedEventArgs(SearchResult result)
		{
			Result = result;
		}

		public SearchResult Result
		{
			get;
			private set;
		}
	}
}
