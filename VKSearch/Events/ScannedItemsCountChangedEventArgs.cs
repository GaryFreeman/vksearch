﻿using System;
using VKSearch.Data;

namespace VKSearch.Events
{
	internal class ScannedItemsCountChangedEventArgs : EventArgs
	{
		public ScannedItemsCountChangedEventArgs(int scanned, int totalCount, int bypass)
		{
			Bypass = bypass;
			Scanned = scanned;
			TotalCount = totalCount;
		}

		public int Bypass
		{
			get;
			set;
		}

		public int Scanned
		{
			get;
			set;
		}

		public int TotalCount
		{
			get;
			set;
		}
	}
}