﻿namespace VKSearch.Events
{
	internal delegate void SearchCompletedEventHandler(ISearcher sender, SearchCompletedEventArgs e);
}
