﻿using System;
using VKSearch.Data;

namespace VKSearch.Events
{
	internal class FoundClientsCountChangedEventArgs : EventArgs
	{
		public FoundClientsCountChangedEventArgs(int count)
		{
			Count = count;
		}

		public int Count
		{
			get;
			set;
		}
	}
}