﻿namespace VKSearch.Events
{
	internal delegate void StatusChangedEventHandler(ISearcher sender, StatusChangedEventArgs e);
}
