﻿using System;
using VKSearch.Data;

namespace VKSearch.Events
{
	internal class StatusChangedEventArgs : EventArgs
	{
		public StatusChangedEventArgs(Status oldStatus, Status newStatus)
		{
			OldStatus = oldStatus;
			NewStatus = newStatus;
		}

		public Status OldStatus
		{
			get;
			private set;
		}

		public Status NewStatus
		{
			get;
			private set;
		}
	}
}
