﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VKSearch.UI
{
	/// <summary>
	/// Interaction logic for AuthorizationWindow.xaml
	/// </summary>
	public partial class AuthorizationWindow : Window
	{
		public AuthorizationWindow()
		{
			InitializeComponent();
			Browser.Navigating += (o, ev) => {
				if (String.IsNullOrEmpty(ev.Uri.Fragment)) {
					return;
				}

				var fragments = ev.Uri.Fragment.Substring(1).Split('&').Where(f => !String.IsNullOrEmpty(f));
				foreach (var fragment in fragments) {
					var parts = fragment.Split('=');
					var key = parts[0];
					var value = parts[1];

					if (key == "access_token") {
						DialogResult = true;
						AccessToken = value;
					} else if (key == "user_id") {
						UserId = Int64.Parse(value);
					} else if (key == "error_description") {
						DialogResult = false;
						Message = HttpUtility.UrlDecode(value);
					}
				}

				if (DialogResult.HasValue) {
					Close();
				}
			};
		}

		public string AccessToken
		{
			get;
			private set;
		}

		public string Message
		{
			get;
			private set;
		}

		public long UserId
		{
			get;
			private set;
		}
	}
}