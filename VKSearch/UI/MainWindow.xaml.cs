﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;
using VkNet.Model;
using VKSearch.Data;
using VKSearch.Events;
using VKSearch.Misc;

namespace VKSearch.UI
{
    public partial class MainWindow : Window
    {
        private const string DumpDefaultFileName = "Клиенты.txt";
        private readonly ISearcher _searcher;

        public MainWindow()
        {
            InitializeComponent();

            _searcher = new Searcher(this);
            _searcher.FoundClientsCountChanged += (s, e) => {
                Dispatcher.BeginInvoke((Action)(() => {
                    FoundClients.Text = e.Count.ToString();
                }));
            };
            _searcher.SearchCompleted += OnSearchComplete;
            _searcher.ScannedItemsCountChanged += (s, e) => {
                Dispatcher.BeginInvoke((Action)(() => {
                    Bypass.Text = e.Bypass.ToString();

                    var totalCountPart = "";
                    if (e.TotalCount != Int32.MaxValue) {
                        totalCountPart = "/" + e.TotalCount.ToString();
                    }

                    ScannedCommunities.Text = e.Scanned + totalCountPart;
                }));
            };
            _searcher.StatusChanged += (s, e) => {
                Dispatcher.BeginInvoke((Action)delegate () {
                    if (e.OldStatus == Data.Status.Stopped) {
                        FoundClients.Text = "0";
                    }

                    switch (e.NewStatus) {
                        case Data.Status.Paused:
                            Status.Text = "Пауза";
                            break;

                        case Data.Status.PreliminarySearch:
                            Status.Text = "Поиск в сообществах";
                            ScannedCommunities.Text = "<идет предварительный поиск>";
                            break;

                        case Data.Status.SearchingInNewsFeed:
                            Status.Text = "Поиск в новостях";
                            if (Bypass.Text == "<запустите поиск>") {
                                Bypass.Text = "<подождите>";
                            }
                            ScannedCommunities.Text = "<подождите>";
                            break;

                        case Data.Status.SearchingInCommunities:
                            Status.Text = "Поиск в сообществах";
                            if (Bypass.Text == "<запустите поиск>") {
                                Bypass.Text = "<подождите>";
                            }
                            ScannedCommunities.Text = "<подождите>";
                            break;

                        case Data.Status.Stopped:
                            Status.Text = "Поиск остановлен";
                            Bypass.Text = "<запустите поиск>";
                            ScannedCommunities.Text = "<запустите поиск>";
                            FoundClients.Text = "<запустите поиск>";
                            break;
                    }
                });
            };

            DumpFilePath.Text = Path.Combine(Directory.GetCurrentDirectory(), DumpDefaultFileName);

            FillCountries(_searcher.Countries);
        }

        private bool CheckInput()
        {
            if (!_searcher.IsAuthorized) {
                MessageBox.Show(this, "Авторизуйтесь перед началом поиска.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (DumpToFile.IsChecked.Value && String.IsNullOrEmpty(DumpFilePath.Text)) {
                MessageBox.Show(this, "Выберите файл для сохранения результатов поиска.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if ((SearchInBoardComments.IsChecked.Value || SearchInWallComments.IsChecked.Value || SearchInWallPosts.IsChecked.Value) && String.IsNullOrEmpty(CommunitiesSearchQuery.Text.Trim())) {
                MessageBox.Show(this, "Введите строку поиска сообществ.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (SearchInWallPosts.IsChecked.Value && String.IsNullOrEmpty(WallPostsSearchQuery.Text.Trim())) {
                MessageBox.Show(this, "Введите строку поиска записей на стене.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (SearchInBoardComments.IsChecked.Value && String.IsNullOrEmpty(BoardPostsSearchQuery.Text.Trim())) {
                MessageBox.Show(this, "Введите строку поиска записей в обсуждениях.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (SearchInWallComments.IsChecked.Value && String.IsNullOrEmpty(WallCommentsSearchQuery.Text.Trim())) {
                MessageBox.Show(this, "Введите строку поиска комментариев на стене.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (!SearchInWallPosts.IsChecked.Value && !SearchInBoardComments.IsChecked.Value &&
                !SearchInWallComments.IsChecked.Value && !SearchInNewsFeed.IsChecked.Value) {
                MessageBox.Show(this, "Выберите хотя бы один тип записей для поиска.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (SearchInNewsFeed.IsChecked.Value && String.IsNullOrEmpty(NewsFeedSearchQuery.Text.Trim())) {
                MessageBox.Show(this, "Введите строку поиска новостей.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (SearchInNewsFeed.IsChecked.Value) {
                int skipOlderThanDays;
                int skipOlderThanHours;

                Int32.TryParse(SkipOlderThanDaysField.Text, out skipOlderThanDays);
                Int32.TryParse(SkipOlderThanHoursField.Text, out skipOlderThanHours);

                if (skipOlderThanDays == 0 && skipOlderThanHours == 0) {
                    MessageBox.Show(this, "Для поиска по новостям необходимо указать ограничение по времени.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    return false;
                }
            }

            var dumpFile = new FileInfo(DumpFilePath.Text);
            if (dumpFile.Exists && !Extensions.IsFileWritable(dumpFile.FullName)) {
                MessageBox.Show(this, "Невозможно выполнить запись в файл для результатов поиска.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            } else if (!dumpFile.Exists && !Extensions.CanCreateInDirectory(dumpFile.DirectoryName)) {
                MessageBox.Show(this, "Невозможно создать файл для результатов поиска.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return false;
            }

            return true;
        }

        private void FillCountries(IEnumerable<Country> countries)
        {
            var comboBoxItems = from c in countries
                                select new ComboBoxItem {
                                    Content = c.Title,
                                    Tag = c.Id
                                };

            foreach (var comboBoxItem in comboBoxItems) {
                Country.Items.Add(comboBoxItem);
            }
        }

        private void NumbersOnly_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var @char = e.Key.ToChar();
            e.Handled = !((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)) &&
                @char != '\0' && @char != '\b' && @char != '\r' && e.Key != Key.Tab;
        }

        private void NumbersOnly_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex(@"[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            _searcher.Stop();
        }

        private void OnCountrySelected(object sender, SelectionChangedEventArgs e)
        {
            if (City == null) {
                return;
            }

            while (City.Items.Count > 1) {
                City.Items.RemoveAt(1);
            }
            City.SelectedIndex = 0;

            var selectedItem = Country.SelectedItem as ComboBoxItem;
            var countryId = selectedItem?.Tag as long?;
            if (countryId == null) {
                City.IsEnabled = false;
                return;
            }

            var cities = _searcher.GetCities((int)countryId.Value);
            foreach (var city in cities) {
                var item = new ComboBoxItem {
                    Content = city.Title,
                    Tag = city.Id
                };
                item.Tag = city.Id;
                item.Content = city.Title;

                City.Items.Add(item);
            }

            City.IsEnabled = true;
        }

        private void OnLoginButtonClick(object sender, RoutedEventArgs e)
        {
            const int scope = 0x1000 | 0x2000 | 0x2 | 0x10000; // messages | wall | friends | offline

            var redirectUri = HttpUtility.UrlEncode("https://oauth.vk.com/blank.html");
            var authorizationWindow = new AuthorizationWindow {
                Owner = this,
                ShowInTaskbar = false,
                Browser = {
                    Source =
                        new Uri(
                            $"https://oauth.vk.com/authorize?client_id=5493446&redirect_uri={redirectUri}&display=popup&scope={scope}&response_type=token&v=5.53&revoke=1")
                }
            };
            var result = authorizationWindow.ShowDialog();
            if (result.Value) {
                _searcher.Authorize(authorizationWindow.AccessToken, authorizationWindow.UserId);
                AccountInfo.Text = $"Вы вошли в аккаунт {_searcher.AccountName}.";
            } else if (result.HasValue && !String.IsNullOrEmpty(authorizationWindow.Message)) {
                MessageBox.Show(authorizationWindow.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OnSearchButtonClick(object sender, RoutedEventArgs e)
        {
            if (!CheckInput()) {
                return;
            }

            var skipOlderThanDays = 0;
            var skipOlderThanHours = 0;

            try {
                skipOlderThanDays = Int32.Parse(SkipOlderThanDaysField.Text);
                skipOlderThanHours = Int32.Parse(SkipOlderThanHoursField.Text);
            } catch (FormatException) {
            }

            var skipOlderThan = DateTime.MinValue;
            if (skipOlderThanDays != 0 || skipOlderThanHours != 0) {
                skipOlderThan = DateTime.Now - new TimeSpan(skipOlderThanDays, skipOlderThanHours, 0, 0);
            }

            var exceptions = from ex in ExceptionsField.Text.Replace('\r', ' ').Split(',')
                             where !String.IsNullOrEmpty(ex)
                             select ex.Trim();
            var communitiesSearchQueries = from q in CommunitiesSearchQuery.Text.Replace('\r', ' ').Split(',')
                                           where !String.IsNullOrEmpty(q)
                                           select q.Trim();
            var boardCommentsSearchQueries = from bp in BoardPostsSearchQuery.Text.Replace('\r', ' ').Split(',')
                                             where !String.IsNullOrEmpty(bp)
                                             select bp.Trim();
            var boardTopicsSearchQueries = from bt in TopicsSearchQuery.Text.Replace('\r', ' ').Split(',')
                                           where !String.IsNullOrEmpty(bt)
                                           select bt.Trim();
            var wallCommentsSearchQueries = from wc in WallCommentsSearchQuery.Text.Replace('\r', ' ').Split(',')
                                            where !String.IsNullOrEmpty(wc)
                                            select wc.Trim();
            var wallPostsSearchQueries = from wp in WallPostsSearchQuery.Text.Replace('\r', ' ').Split(',')
                                         where !String.IsNullOrEmpty(wp)
                                         select wp.Trim();
            var newsFeedSearchQueries = from nf in NewsFeedSearchQuery.Text.Replace('\r', ' ').Split(',')
                                        where !String.IsNullOrEmpty(nf)
                                        select nf.Trim();

            var searchItems = SearchItems.None;
            if (SearchInBoardComments.IsChecked.Value) {
                searchItems |= SearchItems.BoardComments;
            }
            if (SearchInWallPosts.IsChecked.Value) {
                searchItems |= SearchItems.WallPosts;
            }
            if (SearchInWallComments.IsChecked.Value) {
                searchItems |= SearchItems.WallComments;
            }
            if (SearchInNewsFeed.IsChecked.Value) {
                searchItems |= SearchItems.NewsFeed;
            }

            var skipWithAttachments = Filtering.Attachments.None;
            if (SkipWithAlbums.IsChecked.Value) {
                skipWithAttachments |= Filtering.Attachments.Album;
            }
            if (SkipWithAudios.IsChecked.Value) {
                skipWithAttachments |= Filtering.Attachments.Audio;
            }
            if (SkipWithPhotos.IsChecked.Value) {
                skipWithAttachments |= Filtering.Attachments.Photo;
            }
            if (SkipWithVideos.IsChecked.Value) {
                skipWithAttachments |= Filtering.Attachments.Video;
            }

            var searchContext = new SearchContext {
                BoardCommentsSearchQueries = boardCommentsSearchQueries.ToArray(),
                CityId = (City.SelectedItem as ComboBoxItem).Tag as long?,
                CommunitiesSearchQueries = communitiesSearchQueries.ToArray(),
                CountryId = (Country.SelectedItem as ComboBoxItem).Tag as long?,
                DumpFile = DumpToFile.IsChecked.Value ? new FileInfo(DumpFilePath.Text) : null,
                Filtering = new Filtering {
                    BoardTopicsSearchQueries = boardTopicsSearchQueries.ToArray(),
                    Exceptions = exceptions.ToArray(),
                    SkipOlderThan = skipOlderThan,
                    SkipWithAttachments = skipWithAttachments
                },
                Loop = ContinuousSearch.IsChecked.Value,
                NewsFeedSearchQueries = newsFeedSearchQueries.ToArray(),
                PreventDuplicates = PreventDuplicates.IsChecked.Value,
                SearchItems = searchItems,
                WallCommentsSearchQueries = wallCommentsSearchQueries.ToArray(),
                WallPostsSearchQueries = wallPostsSearchQueries.ToArray()
            };

            SearchButton.IsEnabled = false;
            CancelButton.IsEnabled = true;
            Login.IsEnabled = false;

            _searcher.Start(searchContext);
        }

        private void OnSearchComplete(ISearcher sender, SearchCompletedEventArgs e)
        {
            SearchButton.IsEnabled = true;
            CancelButton.IsEnabled = false;
            Login.IsEnabled = true;

            var searchResult = e.Result as SearchResult;
            switch (searchResult.Code) {
                case SearchResult.ResultCode.Cancelled:
                    MessageBox.Show(this, "Поиск отменен.", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;

                case SearchResult.ResultCode.CaptchaFailed:
                    MessageBox.Show(this, "Вы превысили максимальное количество попыток для ввода капчи. Поиск остановлен.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;

                case SearchResult.ResultCode.IncorrectLoginOrPassword:
                    MessageBox.Show(this, "Неверный логин или пароль.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;

                case SearchResult.ResultCode.TooManyRequests:
                    MessageBox.Show(this, "Превышено максимальное количество запросов в секунду. Попробуйте увлеличить интервал. Поиск остановлено.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;

                case SearchResult.ResultCode.UnknownError:
                    MessageBox.Show(this, $"Неизвестная ошибка: {searchResult.Message}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;

                case SearchResult.ResultCode.WorkCompleted:
                    MessageBox.Show(this, "Поиск успешно завершен.", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
            }
        }

        private void OnSelectDumpFilePathClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new SaveFileDialog {
                Filter = "Текстовый файл (*.txt)|*.txt",
                InitialDirectory = Directory.GetCurrentDirectory(),
                Title = "Сохранить файл с результатами поиска как..."
            };
            if (openFileDialog.ShowDialog().Value) {
                DumpFilePath.Text = openFileDialog.FileName;
            }
        }
    }
}