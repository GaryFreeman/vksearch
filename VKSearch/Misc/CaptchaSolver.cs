﻿using System;
using System.Windows;
using VkNet.Utils.AntiCaptcha;
using VKSearch.UI;

namespace VKSearch.Misc
{
	internal class CaptchaSolver : ICaptchaSolver
	{
		private readonly object _captchaIsFalseMutex = new object();
		private readonly Window _mainWindow;
		private bool _captchaIsFalse = false;

		public CaptchaSolver(Window mainWindow)
		{
			this._mainWindow = mainWindow;
		}

		public void CaptchaIsFalse()
		{
			lock (_captchaIsFalseMutex) {
				_captchaIsFalse = true;
			}
		}

		public void ResetCaptchaIsFalse()
		{
			lock (_captchaIsFalseMutex) {
				_captchaIsFalse = false;
			}
		}

		public string Solve(string url)
		{
			string captchaKey = null;

			_mainWindow.Dispatcher.Invoke((Action)delegate () {
				var captchaWindow = new CaptchaWindow();
				captchaWindow.ImageUri = new Uri(url, UriKind.Absolute);
				captchaWindow.Owner = _mainWindow;
				captchaWindow.ShowInTaskbar = false;
				lock (_captchaIsFalseMutex) {
					captchaWindow.CaptchaIsFalseLabel.Visibility = _captchaIsFalse ? Visibility.Visible : Visibility.Collapsed;
				}
				captchaWindow.ShowDialog();

				captchaKey = captchaWindow.CaptchaKeyField.Text;
			});

			return captchaKey;
		}
	}
}