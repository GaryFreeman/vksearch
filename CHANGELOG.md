# Change Log
Все существенные изменения в этом проекте будут описаны в этом файле.
Этот проект следует [Семантическому Версионированию](http://semver.org/).

## [Не выпущено]

## [1.0.0] - 2016-08-13
### Добавлено
- Базовая функциональность.

[Не выпущено]: https://bitbucket.org/GaryFreeman/vksearch.git/branches/compare/develop..master
[1.0.0]: https://bitbucket.org/GaryFreeman/vksearch.git/branches/compare/v1.0.0..a4c1a64
