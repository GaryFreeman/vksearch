﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using VkNet.Model.Attachments;
using VkNet.Utils;

namespace VkNet.Model
{
	[Serializable]
	public class BoardComment : IVkModel
	{
		public ReadOnlyCollection<Attachment> Attachments
		{
			get;
			set;
		}

		public long CommunityId
		{
			get;
			set;
		}

		public DateTime? Date
		{
			get;
			set;
		}

		public long FromId
		{
			get;
			set;
		}

		public long Id
		{
			get;
			set;
		}

		public Likes Likes
		{
			get;
			set;
		}

		public string Text
		{
			get;
			set;
		}

		public long TopicId
		{
			get; set;
		}

		public string TopicUrl
		{
			get;
			set;
		}

		public static BoardComment FromJson(VkResponse response)
		{
			var boardComment = new BoardComment {
				Id = response["id"],
				FromId = response["from_id"],
				Date = response["date"],
				Text = response["text"],
				Attachments = response["attachments"].ToReadOnlyCollectionOf<Attachment>(x => x),
				Likes = response["likes"]
			};

			return boardComment;
		}

		IVkModel IVkModel.FromJson(VkResponse response)
		{
			return FromJson(response);
		}
	}
}