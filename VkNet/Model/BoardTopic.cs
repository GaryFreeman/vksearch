﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using VkNet.Model.Attachments;
using VkNet.Utils;

namespace VkNet.Model
{
	[Serializable]
	public class BoardTopic : MediaAttachment, IVkModel
	{
		static BoardTopic()
		{
			RegisterType(typeof(BoardTopic), "topic");
		}

		public long Comments
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public long CreatedBy
		{
			get;
			set;
		}

		public new long Id
		{
			get;
			set;
		}

		public bool IsClosed
		{
			get;
			set;
		}

		public bool IsFixed
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		public DateTime? Updated
		{
			get;
			set;
		}

		public long UpdatedBy
		{
			get;
			set;
		}

		public static BoardTopic FromJson(VkResponse response)
		{
			var boardTopic = new BoardTopic {
				Id = response["id"] ?? response["tid"],
				Title = response["title"],
				Comments = response["comments"],
				Created = response["created"],
				CreatedBy = response["created_by"],
				IsClosed = response["is_closed"],
				IsFixed = response["is_fixed"],
				Updated = response["updated"],
				UpdatedBy = response["updated_by"]
			};

			(boardTopic as MediaAttachment).Id = boardTopic.Id;

			return boardTopic;
		}

		IVkModel IVkModel.FromJson(VkResponse response)
		{
			return FromJson(response);
		}
	}
}